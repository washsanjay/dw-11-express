import { Router } from "express";
import { ClassRoom } from "../schema/model.js";

export let classRoomsRouter = Router()

classRoomsRouter.route('/')
.get(async(req,res,next) => {
    try {
        let result = await ClassRoom.find()
        res.json({
            success:true,
             message:"ClassRoom Read successfully",
             result})
    } catch (error) {
        res.json({
            success:false,
            message:error
        })
    }

  
})
.post(async(req,res,next) => {
   // console.log(req.body)
   let data = req.body
   
   try {
    let result = await ClassRoom.create(data)
    res.json({success:true, message:"ClassRoom create successfully"})
   } catch (error) {
    res.json({success:false, message:error.message})
   }
   
    
})

classRoomsRouter.route('/:id')
.get(async(req, res, next)=> {
    let id = req.params.id
    //console.log("id is: ",id)
    try {
        let result = await ClassRoom.findById(id)
        res.json({success:true, message:"ClassRoom Read successfully",result:result})
    } catch (error) {
       // res.json({success:false, message:error.message})
    }
   
   
})
.delete(async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await ClassRoom.findByIdAndDelete(id)

        res.json({success:true, message:"ClassRoom deleted successfully"})
    } catch (error) {
        res.json({success:false, message:error.message})
        
    }
   
})
.patch(async(req,res,next) => {
    let id = req.params.id
    let data = req.body
    try {
        let result = await ClassRoom.findByIdAndUpdate(id,data,{new: true})
    res.json({success:true, message:"ClassRoom Updated successfully",result})


    } catch (error) {
    res.json({success:false, message:error.message})
        
    }
})