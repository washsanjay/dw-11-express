import { Router } from "express";
import { createContact, deleteContact, getAllContacts, getOneContact, updateContact } from "../controller/contactController.js";

export let contactRouters = Router()
contactRouters.route('/')
.get(getAllContacts)
.post(createContact)

contactRouters.route('/:id')
//GET /api/v1/contacts/:id - Get contact information with specified ID
.get(getOneContact)
//PUT /api/v1/contacts/:id - Update an existing contact
.patch(updateContact)
//DELETE /api/v1/contacts/:id - Delete a contact
.delete(deleteContact);

