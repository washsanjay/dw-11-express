import { Router } from "express";
import { createProduct, deleteProduct, getAllProducts, getOneProduct, updateProduct } from "../controller/productController.js";
export let productRouter = Router()
productRouter.route('/')
.get(getAllProducts)
.post(createProduct)

productRouter.route('/:id')
//GET /api/v1/Products/:id - Get Product information with specified ID
.get(getOneProduct)
//PUT /api/v1/Products/:id - Update an existing Product
.patch(updateProduct)
//DELETE /api/v1/Products/:id - Delete a Product
.delete(deleteProduct);

