import { Router } from "express";

export let schoolsRouter = Router()

schoolsRouter.route('/')
.get((req,res,next) => {
    res.json({success:true, message:"schools read successfully"})
})
.post((req,res,next) => {
    res.json({success:true, message:"schools created successfully"})
})
.delete((req,res,next) => {
    res.json({success:true, message:"schools deleted successfully"})
})
.patch((req,res,next) => {
    res.json({success:true, message:"schools updated successfully"})
})