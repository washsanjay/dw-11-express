import { Router } from "express";

export let firstRouter = Router()

firstRouter.route("/")
.post((req, res, next)=>{
  console.log('I m first middleware ')
  req.name = 'Sanjay'
  next()
},
(req, res, next)=>{
   console.log(`Hello ${req.name}`)
   res.json({message:`Hello ${req.name}`})
   console.log('I m second middleware ')
   next()
 },
 (req, res, next)=>{
   console.log('I m third middleware ')
   //next()
 })

// .get((req, res, next)=>{
//    res.json("bike Get")
// })
.get((req, res, next)=>{
   console.log('i am middleware 1')
   next()
},
(err, req, res, next)=>{
   console.log(err.message)
   console.log('i am error  middleware 2')
   let error = new Error('my error')
   next(error)
},
(req, res, next)=>{
   console.log('i am middleware 2')
})

.delete((req, res, next)=>{
   res.json("bike Delete")
})

.patch((req, res, next)=>{
   let data =req.body
   //console.log(data)
   res.json(data)
})
