import { Router } from "express"
import { uploadMultipleFiles, uploadSingleFile } from "../controller/fileController.js"
import upload from "../middleware/uploadFile.js"


export let fileRouter = Router()
fileRouter
.route("/single")
.post(upload.single("profileImage"),uploadSingleFile)

fileRouter
.route("/multiple")
.post(upload.array("profileImage"),uploadMultipleFiles)