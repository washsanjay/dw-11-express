import { Router } from "express";
import { College } from "../schema/model.js";

export let collegesRouter = Router()

collegesRouter.route('/')
.get(async(req,res,next) => {
    try {
        let result = await College.find()
        res.json({
            success:true,
             message:"College Read successfully",
             result})
    } catch (error) {
        res.json({
            success:false,
            message:error
        })
    }

  
})
.post(async(req,res,next) => {
   // console.log(req.body)
   let data = req.body
   
   try {
    let result = await College.create(data)
    res.json({success:true, message:"College create successfully"})
   } catch (error) {
    res.json({success:false, message:error.message})
   }
   
    
})

collegesRouter.route('/:id')
.get(async(req, res, next)=> {
    let id = req.params.id
    //console.log("id is: ",id)
    try {
        let result = await College.findById(id)
        res.json({success:true, message:"College Read successfully",result:result})
    } catch (error) {
       // res.json({success:false, message:error.message})
    }
   
   
})
.delete(async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await College.findByIdAndDelete(id)

        res.json({success:true, message:"College deleted successfully"})
    } catch (error) {
        res.json({success:false, message:error.message})
        
    }
   
})
.patch(async(req,res,next) => {
    let id = req.params.id
    let data = req.body
    try {
        let result = await College.findByIdAndUpdate(id,data,{new: true})
    res.json({success:true, message:"College Updated successfully",result})


    } catch (error) {
    res.json({success:false, message:error.message})
        
    }
})