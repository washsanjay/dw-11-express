import { Router } from "express";

export let firstRouter = Router()
firstRouter
.route("/")
.get( (req, res, next) => {
   // console.log("i am at home get")
   res.json({
      success: true,
      message: "get successfully"
   })
})
.post((req,res,next)=>{
   // console.log("i am at home post")
   res.json({
      success: true,
      message: "post successfully"
   })
})
.patch(() => {
    console.log("i am at home patch")
})
.delete(()=> {
    console.log("I'm home deleted!")
})

firstRouter
.route("/name")
.get( (req, res) => {
   console.log("i am at name get")
})
.post((req,res)=>{
   console.log("i am at name post")
})
.patch(() => {
   console.log("i am at name patch")
})
.delete(()=> {
   console.log("I'm deleted!")
})
firstRouter
.route("/address")
.get( (req, res) => {
   console.log("i am at address get")
})
.post((req,res)=>{
   console.log("i am at address post")
})
.patch(() => {
   console.log("i am at address patch")
})
.delete(()=> {
   console.log("I'm deleted! address")
})

firstRouter
.route("/age")
.get( (req, res) => {
   console.log("i am at age get")
})
.post((req,res)=>{
   console.log("i am at age post")
})
.patch(() => {
   console.log("i am at age patch")
})
.delete(()=> {
   console.log("I'm deleted! age")
})

firstRouter
.route("/height")
.get( (req, res) => {
   console.log("i am at height get")
})
.post((req,res)=>{
   console.log("i am at height post")
})
.patch(() => {
   console.log("i am at height patch")
})
.delete(()=> {
   console.log("I'm deleted! height")
})
