import { Router } from "express";

export let departmentsRouter = Router()

departmentsRouter.route('/')
.get((req,res,next) => {
   
    res.json({success:true, message:"department read successfully"})
})
.post((req,res,next) => {
    console.log(req.query)
    console.log(req.method)
    res.json({success:true, message:"department created successfully"})
})
.delete((req,res,next) => {
    res.json({success:true, message:"department deleted successfully"})
})
.patch((req,res,next) => {
    res.json({success:true, message:"department updated successfully"})
})

departmentsRouter.route('/:id')
.get((req, res, next)=> {
   res.json({"sucess": true})
})

