import { Router } from "express";
import { Student } from "../schema/model.js";
import { createBlog, deleteBlogById, getAllBlogs, getBlogById, updateBlogById } from "../controller/blogController.js";

export let blogsRouter = Router()

blogsRouter.route('/')
.get(getAllBlogs)
.post(createBlog)

blogsRouter.route('/:id')
.get(getBlogById)
.delete(deleteBlogById)
.patch(updateBlogById)