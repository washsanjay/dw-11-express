import { Router } from "express";
// import { createProduct, deleteProduct, getAllProducts, getOneProduct, updateProduct } from "../controller/productController.js";
import { createReview, deleteReview, getAllReviews, getOneReview, updateReview } from "../controller/reviewController.js";
export let reviewRouter = Router()
reviewRouter.route('/')
.get(getAllReviews)
.post(createReview)

reviewRouter.route('/:id')
//GET /api/v1/Products/:id - Get Product information with specified ID
.get(getOneReview)
//PUT /api/v1/Products/:id - Update an existing Product
.patch(updateReview)
//DELETE /api/v1/Products/:id - Delete a Product
.delete(deleteReview);

