import { Router } from "express";
import { createStudentImg, deleteStudentImg, getAllStudentImgs, getOneStudentImg, updateStudentImg } from "../controller/studentImgController.js";
import upload from "../middleware/uploadFile.js";
export let studentImgRouter = Router()
studentImgRouter.route('/')
.get(getAllStudentImgs)
.post(upload.single("file"), createStudentImg)

studentImgRouter.route('/:id')
//GET /api/v1/studentImgs/:id - Get studentImg information with specified ID
.get(getOneStudentImg)
//PUT /api/v1/studentImgs/:id - Update an existing studentImg
.patch(updateStudentImg)
//DELETE /api/v1/studentImgs/:id - Delete a studentImg
.delete(deleteStudentImg);

