import { Router } from "express";
import { Student } from "../schema/model.js";
import { createStudent, deleteStudentById, getAllStudents, getStudentById, updateStudentById } from "../controller/studentController.js";
// import { getAllStudents, createStudent, getStudentById } from "../controller/studentController.js";

export let studentsRouter = Router()

studentsRouter.route('/')
.get(getAllStudents)
.post(createStudent)

studentsRouter.route('/:id')
.get(getStudentById)
.delete(deleteStudentById)
.patch(updateStudentById)