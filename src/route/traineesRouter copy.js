import { Router } from "express";
import { Trainee } from "../schema/model.js";

export let traineesRouter = Router()

traineesRouter.route('/')
.get(async(req,res,next) => {
    try {
        let result = await Trainee.find()
        res.json({
            success:true,
             message:"Trainee Read successfully",
             result})
    } catch (error) {
        res.json({
            success:false,
            message:error
        })
    }

  
})
.post(async(req,res,next) => {
   // console.log(req.body)
   let data = req.body
   
   try {
    let result = await Trainee.create(data)
    res.json({success:true, message:"Trainee create successfully"})
   } catch (error) {
    res.json({success:false, message:error.message})
   }
   
    
})

traineesRouter.route('/:id')
.get(async(req, res, next)=> {
    let id = req.params.id
    //console.log("id is: ",id)
    try {
        let result = await Trainee.findById(id)
        res.json({success:true, message:"Trainee Read successfully",result:result})
    } catch (error) {
       // res.json({success:false, message:error.message})
    }
   
   
})
.delete(async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await Trainee.findByIdAndDelete(id)

        res.json({success:true, message:"Trainee deleted successfully"})
    } catch (error) {
        res.json({success:false, message:error.message})
        
    }
   
})
.patch(async(req,res,next) => {
    let id = req.params.id
    let data = req.body
    try {
        let result = await Trainee.findByIdAndUpdate(id,data,{new: true})
    res.json({success:true, message:"Trainee Updated successfully",result})


    } catch (error) {
    res.json({success:false, message:error.message})
        
    }
})