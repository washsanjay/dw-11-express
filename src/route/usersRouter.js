import { Router } from "express";
import { createUser, deleteUser, getAllUsers, getOneUser, updateUser } from "../controller/userController.js";
// import { createProduct, deleteProduct, getAllProducts, getOneProduct, updateProduct } from "../controller/productController.js";
export let userRouter = Router()
userRouter.route('/')
.get(getAllUsers)
.post(createUser)

userRouter.route('/:id')
//GET /api/v1/Products/:id - Get Product information with specified ID
.get(getOneUser)
//PUT /api/v1/Products/:id - Update an existing Product
.patch(updateUser)
//DELETE /api/v1/Products/:id - Delete a Product
.delete(deleteUser);

