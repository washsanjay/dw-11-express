import { Router } from "express";
import { Teacher } from "../schema/model.js";

export let teachersRouter = Router()

teachersRouter.route('/')
.get(async(req,res,next) => {
    try {
        let result = await Teacher.find()
        res.json({
            success:true,
             message:"Student Read successfully",
             result})
    } catch (error) {
        res.json({
            success:false,
            message:error
        })
    }

  
})
.post(async(req,res,next) => {
   // console.log(req.body)
   let data = req.body
   
   try {
    let result = await Teacher.create(data)
    res.json({success:true, message:"Student create successfully"})
   } catch (error) {
    res.json({success:false, message:error.message})
   }
   
    
})

teachersRouter.route('/:id')
.get(async(req, res, next)=> {
    let id = req.params.id
    //console.log("id is: ",id)
    try {
        let result = await Teacher.findById(id)
        res.json({success:true, message:"Student Read successfully",result:result})
    } catch (error) {
       // res.json({success:false, message:error.message})
    }
   
   
})
.delete(async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await Teacher.findByIdAndDelete(id)

        res.json({success:true, message:"Student deleted successfully"})
    } catch (error) {
        res.json({success:false, message:error.message})
        
    }
   
})
.patch(async(req,res,next) => {
    let id = req.params.id
    let data = req.body
    try {
        let result = await Teacher.findByIdAndUpdate(id,data,{new: true})
    res.json({success:true, message:"Student Updated successfully",result})


    } catch (error) {
    res.json({success:false, message:error.message})
        
    }
})



