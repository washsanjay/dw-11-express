import { StudentImg } from "../schema/model.js";

export let createStudentImg = async(req, res, next) => {
    let link = `localhost:8081/${req.file.filename}`
    
    //let data = req.body
    try{
        const StudentImg = await StudentImg.create({
            name:req.body.name,
            age:req.body.age,
            file: link
        });
        if(!StudentImg){
            return res.status(400).json({success: false, msg:'Failed to add new StudentImg'})
            }else{
                console.log('New StudentImg added')
                return res.status(201).json({success: true,msg:"StudentImg created!",StudentImg})
                    }


}
catch (e) {
    console.error(e);
}
};

export let getAllStudentImgs = async(req, res, next) => {
    let limit = req.query.limit
    let page=req.query.page
    try{
        const StudentImgs =  await StudentImg.find().skip((page-1)*limit).limit(limit); //pagination
        // Sending back a response with the list of StudentImgs in it
         res.status(200).json({success: true, msg:'StudentImg successfully fetched',StudentImgs});
        } catch (err) {
            res.json({success:false, message:err.message})
            }
}

// get specified StudentImgs

export let getOneStudentImg = async (req,res,next)=>{
    let id=req.params.id;
    try{
      
        const StudentImg =await StudentImg.findById(id);
        if (!StudentImg){
            return res.status(404).json({ success :false ,msg:`No user found with the id ${id}`})
            } else{
                return res.status(200).json({success:true,msg:`StudentImg detail fetched successfully`,StudentImg})
                
            }
        }
        catch(err){
            console.error(err.message)
        }

}

//update specified StudentImg
export let updateStudentImg=async(req,res,next)=> {
    const id = req.params.id;
    const updates = req.body;
    console.log(updates)
    try {
    let result = await StudentImg.findByIdAndUpdate(id,updates,{new: true})
    res.json({success:true, message:"StudentImg Updated successfully",result})


    } catch (error) {
        console.log("error ",updates)
    res.json({success:false, message:error.message})
        
    }
    }

    //delete specified StudentImg
    export let deleteStudentImg =async (req,res,next) =>{
        const id = req.params.id;
        try {
            let result = await StudentImg.findByIdAndDelete(id)
            if (result === null){
                res.json({
                    success: false,
                    message: "No StudentImg with the given ID was found."
                })
            }else {
            res.json({success:true, message:"StudentImg deleted successfully",result})
    
            }
    
        } catch (error) {
            res.json({success:false, message:error.message})
            
        }
}