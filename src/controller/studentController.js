import { Student } from "../schema/model.js"

export let getAllStudents = async(req,res,next) => {
    //while searching we focus on value not on type of data
    // roll greater than 18
    /* let result = await Student.find({roll:{$gt:20}}) 
       find({roll:{$gte:18}})
       let result = await Student.find({roll:{$lt:18}})
       let result = await Student.find({roll:{$lte:18} })
       let result = await Student.find({roll:{$ne:18} })
       let result = await Student.find({roll: {$in:[20,25,30]}})
       let result = await Student.find({roll:{$gte:20,$lte:25}})  range

       //string searching
       let stringResult=await Student.find({name:/^sadam/})   starts
       stringResult=await Student.find({name:/dam$/})     ends
       stringResult=await Student.find({name:/dam/i})      insensitive case
       stringResult = await Student.find({name:{$in:["nitan","ram"]})
      */
        /* 
        
        */


       
    try {
       // let result = await Student.find()
       //select
        let result = await Student.find({}).select("name age -_id")  //exceptional case
        //let result = await Student.find({}).select("name -age ")//not valid
        // in select either use all (-) or all (+) but dont combine both(+ and -) except for _id
        //find has control over the object where as select has control over object property

       

        /* 
        The order 
         //find  sort select skip limit
        */
        //sorting
        /* 
          .find({}).sort("name")
          .find({}).sort("-name")
          .find({}).sort("name age")
          .find({}).sort("name -age")
          .find({}).sort("-name age")
          .find({}).sort("age -name")
        */
        //skip
        //find({}).skip("3")
        //limit
        //.find({}).limit("4")


       // let result = await Student.find({name:"jeni",roll:20})
        res.json({
            success:true,
             message:"Student Read successfully",
             result})
    } catch (error) {
        res.json({
            success:false,
            message:error
        })
    }

  
}
export let createStudent = async(req,res,next) => {
    // console.log(req.body)
    let data = req.body
    
    try {
     let result = await Student.create(data)
     res.json({success:true, message:"Student create successfully",result})
    } catch (error) {
     res.json({success:false, message:error.message})
    }
    
     
 }
 export let getStudentById = async(req, res, next)=> {
    let id = req.params.id
    //console.log("id is: ",id)
    try {
        let result = await Student.findById(id)
        res.json({success:true, message:"Student Read successfully",result:result})
    } catch (error) {
       // res.json({success:false, message:error.message})
    }
   
   
}

export let deleteStudentById = async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await Student.findByIdAndDelete(id)
        if (result === null){
            res.json({
                success: false,
                message: "No student with the given ID was found."
            })
        }else {
        res.json({success:true, message:"Student deleted successfully",result})

        }

    } catch (error) {
        res.json({success:false, message:error.message})
        
    }
   
}

export let updateStudentById = async(req,res,next) => {
    let id = req.params.id
    let data = req.body
    try {
        let result = await Student.findByIdAndUpdate(id,data,{new: true})
        //if new:false it gives old data
        //if new:true it gives new data
    res.json({success:true, message:"Student Updated successfully",result})


    } catch (error) {
    res.json({success:false, message:error.message})
        
    }
}
/* 
{
    "name": "c",
    "age": 33,
    "password":"Password@123",
    "phoneNumber": 1111111111,
    "roll": 2,
    "isMarried": false,
    "spouseName": "lkjl",
    "email": "abc@gmai",
    "gender": "male",
    "dob": "2019-2-2",
    "location": {
        "country": "nepal",
        "exactLocation": "gagalphedi"
    },
    "favTeacher": [
        "a",
        "b",
        "c",
        "nitan"
    ],



    "favSubject": [
        {
            "bookName": "javascript",
            "bookAuthor": "nitan"
        },
        {
            "bookName": "b",
            "bookAuthor": "b"
        }
    ],



} */
// export let createStudent = 

//  export let getStudentById = 