import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { secretKey } from "../constant.js"
import { WebUser } from "../schema/model.js"
import { sendEmail } from "../utils/sendMail.js"

export let createWebUser = async(req,res,next)=>{
 try {
    let data = req.body
//    const userExist=await WebUser.findOne({email:data.email})
//    if (userExist) return res.status(409).json("User already
//    exists")  //Conflict
//    const salt = await bcrypt.genSalt(10);
//    const hashedPassword = await bcrypt.hash(data.password,salt)
//    data={...data , password : hashedPassword}
//    const newUser = new WebUser(data)
//    const result = await newUser.save()
//    const token = jwt.sign({_id :result._id},secretKey,{expiresIn:"2h"});
//    res.status(201).header('x-auth-token',token).json(result)
//  } catch (error) {
//      console.log(error)
//      res.status(500).json("Internal Server Error")
//  }
// }




    let hashPassword = await bcrypt.hash(data.password, 10)

    data = {
        ...data, //sab data
        isVerifiedEmail:false,
        password:hashPassword,
    }
    let result = await WebUser.create(data) 
    //send mail with link
    //generate token
    let infoObj={
        _id:result._id,
    }
    let expiryInfo ={
        expiresIn: "365d",
    }
        let token = jwt.sign(infoObj, secretKey, expiryInfo)
    //link (front end ko)
    await sendEmail({
        from: "no reply <washsanjay@gmail.com",
        to: [data.email],  //email should be genuine in postman
        subject:"Account created",
        html:`
       <h1>Your account has been created successfully</h1>
       <a href ="http://localhost:8081/verify-email?token=${token}">
       http://localhost:8081/verify-email?token=${token}

       </a>
    `
    })
    
    //send mail

    res.json({
        success:true,
        message: "User created successfully",
        data:result,
    })
} catch (error) {
    res.json({
        success:false,
        message:error.message
    })
 }
}

export let verifyEmail = async(req,res,next)=>{
   
    try {
        let tokenString = req.headers.authorization //token pathauna paryo vne
        let tokenArray = tokenString.split(" ")
        let token = tokenArray[1]
        console.log(token)
        // console.log(tokenString.split(" ")[1])
    
        //verify token
            let infoObj = jwt.verify(token, secretKey)//either throw error or verify vyo vne info obj
            let id = infoObj._id //get_id form token
            console.log(infoObj)

            let result = await WebUser.findByIdAndUpdate(id,{
                isVerifiedEmail:true,
            },
            {
                new:true,
            })
            res.json({
                success:true,
                message:"User verified successfully",
                result:result,
            })
            
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
}

export let loginUser = async (req,res,next)=>{
    try {
        let email = req.body.email
        let password = req.body.password

        let user = await WebUser.findOne({email:email}) //output in array
        //findOne ko output object ma aauxa natra null
        if(user){
            if(user.isVerifiedEmail){
                let isValidPassword = await bcrypt.compare(password, user.password) //Output either true or false
                    if(isValidPassword){
                        let infoObj={
                         _id: user._id,
                
                        }
                        let expiryInfo ={
                            expiresIn: "365d",
                        }
                        let token = jwt.sign(infoObj, secretKey, expiryInfo)

                        res.json({
                            success: true,
                            message: "User login successfully",
                            data:token,
                        })

                }
                else{
                    let error = new Error("Credentials doesn't match")
                    throw error
                }


            }
            else{
                let error = new Error ("Credentials doesn't match")
                throw error
            }

        }
        else{
            let error = new Error("Credentials not found")
            throw error
        }
    } catch (error) {
        res.json({
        success: false,
        message: error.message
        })
    }

}

export let myProfile = async (req,res,next)=>{
    try {
        let _id = req. _id
        let  result = await WebUser.findById(_id)
        res.json({
            success:true,
            message: "Profile read successfully",
            data:result,
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:"Unable to read profile"
        })
        
    }

}

export let updateProfile = async(req,res,next)=>{ //sabai update hunxa except email and password
    try {
        let _id = req._id
        let data = req.body
        // console.log(data)
        delete data.email
        delete data.password
        let result =  await WebUser.findByIdAndUpdate(_id,data,{new:true})
        console.log(result)
        res.json({
            success : true,
            message:"Profile updated successfully",
            data:result,
        })
    } catch (error) {
        console.log(error)
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updatePassword = async (req,res,next)=>{
    
        let _id = req._id
        let oldPassword = req.body.oldPassword
        let newPassword = req.body.newPassword
        try{
        let data = await  WebUser.findById(_id)
        let hashPassword = data.password
        let isValidPassword = await bcrypt.compare(oldPassword, hashPassword)
        if(isValidPassword){
            let newHashPassword = await bcrypt.hash(newPassword, 10)
            let result = await WebUser.findByIdAndUpdate(_id, 
                {
                    password:newHashPassword
                },
                {
                    new:true
                })
            res.json({
                success:true,
                message:'Password Updated Successfully',
                data:result

            })

        }
        else{
            let error = new Error("Credentials doesn't match")
            throw error
        }
    } catch (error) {
        res.json({
            success:false,
        message: error.message
        })
    }

}

export let readAllWebUser = async (req,res,next)=>{
    
    try {
        let result = await WebUser.find({})
        res.json({
            success : true ,
            message:"All user reads successfully",
            data:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let readSpecificWebUser = async (req,res,next)=>{
    let id = req.params.id //postman bata leko so params.id
    try {
        let result = await WebUser.findById(id)
        res.json({
            success : true ,
            message:"User read successfully",
            data:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let updateSpecificWebUser = async(req,res,next)=>{
   
    try {
        let id = req.params.id
        let data = req.body
        delete data.email
        delete data.password
        let result =  await WebUser.findByIdAndUpdate(id,data,{new:true})  
        res.json({
            success : true ,
            message:"User updated successfully",
            data:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let deleteSpecificWebUser = async(req,res,next)=>{
    try {
        let id = req.params.id
        let result =  await WebUser.findByIdAndDelete(id)  
        res.json({
            success : true ,
            message:"User deleted successfully",
            data:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }  

}