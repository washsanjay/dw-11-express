export let uploadSingleFile = (req,res,next)=>{
    console.log(req.file)
    res.json({
        success: true,
        message:"File upload successfully",
        link: `http://localhost:8081/${req.file.filename}`
    })
}

export let  uploadMultipleFiles = (req,res,next)=> {
   
  let links =req.files.map((value) => {
   let link = `localhost:8081/${value.filename}`
   return link
  })
   res.json({
        success: true,
        message:"File upload successfully",
        links:links
    })
}
