import { User } from "../schema/model.js";
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendMail.js";

export let createUser = async(req, res, next) => {
    let data = req.body
    let password = data.password
    let hashedPassword = await bcrypt.hash(password,10)
    data.password = hashedPassword
    try{
        const user = await User.create(data);
        if(!user){
            return res.status(400).json({success: false, msg:'Failed to add new user'})
            }else{

                sendEmail({
                    from:"NO REPLY <washsanjay@gmail.com>",
                    to:[data.email],
                    subject:`New Account Created `,
                    html:`<h3>Hey Admin!</h3><p>A new account has been created with the following details.</p><br/>Name : ${data.name}`
                   // html:`<h2>Hello Admin!</h2><p>A new account has been created by ${data.name}. Please verify the account.</p>`
                  
                })
                console.log('New user added')
                return res.status(201).json({success: true,msg:"user created!",user})
                    }


}
catch (e) {
    console.error(e);
}
};

export let getAllUsers = async(req, res, next) => {
   // let limit = req.query.limit
   // let page=req.query.page
    try{
        const users =  await User.find({}); //pagination
        // Sending back a response with the list of users in it
         res.status(200).json({success: true, msg:'user successfully fetched',users});
        } catch (err) {
            res.json({success:false, message:err.message})
            }
}

// get specified users

export let getOneUser = async (req,res,next)=>{
    let id=req.params.id;
    try{
      
        const user =await User.findById(id);
        if (!user){
            return res.status(404).json({ success :false ,msg:`No user found with the id ${id}`})
            } else{
                return res.status(200).json({success:true,msg:`user detail fetched successfully`,user})
                
            }
        }
        catch(err){
            console.error(err.message)
        }

}

//update specified user
export let updateUser=async(req,res,next)=> {
    const id = req.params.id;
    const updates = req.body;
    console.log(updates)
    try {
    let result = await User.findByIdAndUpdate(id,updates,{new: true})
    res.json({success:true, message:"user Updated successfully",result})


    } catch (error) {
        console.log("error ",updates)
    res.json({success:false, message:error.message})
        
    }
    }

    //delete specified user
    export let deleteUser =async (req,res,next) =>{
        const id = req.params.id;
        try {
            let result = await User.findByIdAndDelete(id)
            if (result === null){
                res.json({
                    success: false,
                    message: "No user with the given ID was found."
                })
            }else {
            res.json({success:true, message:"user deleted successfully",result})
    
            }
    
        } catch (error) {
            res.json({success:false, message:error.message})
            
        }
}