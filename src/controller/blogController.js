import { Blog } from "../schema/model.js"

export let getAllBlogs = async(req,res,next) => {
    try {
        let result = await Blog.find()
        res.json({
            success:true,
             message:"Blogs Read successfully",
             result})
    } catch (error) {
        res.json({
            success:false,
            message:error
        })
    }

  
}
export let createBlog = async(req,res,next) => {
    // console.log(req.body)
    let data = req.body
    
    try {
     let result = await Blog.create(data)
     res.json({success:true, message:"Blog create successfully"})
    } catch (error) {
     res.json({success:false, message:error.message})
    }
    
     
 }
 export let getBlogById = async(req, res, next)=> {
    let id = req.params.id
    //console.log("id is: ",id)
    try {
        let result = await Blog.findById(id)
        res.json({success:true, message:"Blog Read successfully",result:result})
    } catch (error) {
       // res.json({success:false, message:error.message})
    }
   
   
}

export let deleteBlogById = async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await Blog.findByIdAndDelete(id)
        if (result === null){
            res.json({
                success: false,
                message: "No Blog with the given ID was found."
            })
        }else {
        res.json({success:true, message:"Blog deleted successfully",result})

        }

    } catch (error) {
        res.json({success:false, message:error.message})
        
    }
   
}

export let updateBlogById = async(req,res,next) => {
    let id = req.params.id
    let data = req.body
    try {
        let result = await Blog.findByIdAndUpdate(id,data,{new: true})
    res.json({success:true, message:"Blog Updated successfully",result})


    } catch (error) {
    res.json({success:false, message:error.message})
        
    }
}
// export let createStudent = 

//  export let getStudentById = 