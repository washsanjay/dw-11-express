import { Contact } from "../schema/model.js";

export let createContact = async(req, res, next) => {
    let data = req.body
    try{
        const contact = await Contact.create(data);
        if(!contact){
            return res.status(400).json({success: false, msg:'Failed to add new contact'})
            }else{
                console.log('New contact added')
                return res.status(201).json({success: true,msg:"Contact created!",contact})
                    }


}
catch (e) {
    console.error(e);
}
};

export let getAllContacts = async(req, res, next) => {
    let limit = req.query.limit
    let page=req.query.page
    try{
        const contacts =  await Contact.find().skip((page-1)*limit).limit(limit); //pagination
        // Sending back a response with the list of contacts in it
         res.status(200).json({success: true, msg:'Contact successfully fetched',contacts});
        } catch (err) {
            res.json({success:false, message:err.message})
            }
}

// get specified contacts

export let getOneContact = async (req,res,next)=>{
    let id=req.params.id;
    try{
      
        const contact =await Contact.findById(id);
        if (!contact){
            return res.status(404).json({ success :false ,msg:`No user found with the id ${id}`})
            } else{
                return res.status(200).json({success:true,msg:`contact detail fetched successfully`,contact})
                
            }
        }
        catch(err){
            console.error(err.message)
        }

}

//update specified contact
export let updateContact=async(req,res,next)=> {
    const id = req.params.id;
    const updates = req.body;
    console.log(updates)
    try {
    let result = await Contact.findByIdAndUpdate(id,updates,{new: true})
    res.json({success:true, message:"Contact Updated successfully",result})


    } catch (error) {
        console.log("error ",updates)
    res.json({success:false, message:error.message})
        
    }
    }

    //delete specified contact
    export let deleteContact =async (req,res,next) =>{
        const id = req.params.id;
        try {
            let result = await Contact.findByIdAndDelete(id)
            if (result === null){
                res.json({
                    success: false,
                    message: "No contact with the given ID was found."
                })
            }else {
            res.json({success:true, message:"contact deleted successfully",result})
    
            }
    
        } catch (error) {
            res.json({success:false, message:error.message})
            
        }
}