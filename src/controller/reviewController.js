import { Review } from "../schema/model.js";

export let createReview = async(req, res, next) => {
    let data = req.body
    try{
        const review = await Review.create(data);
        if(!review){
            return res.status(400).json({success: false, msg:'Failed to add new review'})
            }else{
                console.log('New review added')
                return res.status(201).json({success: true,msg:"review created!",review})
                    }


}
catch (e) {
    console.error(e);
}
};

export let getAllReviews = async(req, res, next) => {
    let limit = req.query.limit
    let page=req.query.page
    try{
        //const reviews =  await Review.find().skip((page-1)*limit).limit(limit); //pagination
        const reviews = await Review.find({}).populate("productId").populate("userId")
        // Sending back a response with the list of reviews in it
         res.status(200).json({success: true, msg:'review successfully fetched',reviews});
        } catch (err) {
            res.json({success:false, message:err.message})
            }
}

// get specified reviews

export let getOneReview = async (req,res,next)=>{
    let id=req.params.id;
    try{
      
        const review =await Review.findById(id);
        if (!review){
            return res.status(404).json({ success :false ,msg:`No review found with the id ${id}`})
            } else{
                return res.status(200).json({success:true,msg:`review detail fetched successfully`,review})
                
            }
        }
        catch(err){
            console.error(err.message)
        }

}

//update specified review
export let updateReview=async(req,res,next)=> {
    const id = req.params.id;
    const updates = req.body;
    console.log(updates)
    try {
    let result = await Review.findByIdAndUpdate(id,updates,{new: true})
    res.json({success:true, message:"review Updated successfully",result})


    } catch (error) {
        console.log("error ",updates)
    res.json({success:false, message:error.message})
        
    }
    }

    //delete specified review
    export let deleteReview =async (req,res,next) =>{
        const id = req.params.id;
        try {
            let result = await Review.findByIdAndDelete(id)
            if (result === null){
                res.json({
                    success: false,
                    message: "No review with the given ID was found."
                })
            }else {
            res.json({success:true, message:"review deleted successfully",result})
    
            }
    
        } catch (error) {
            res.json({success:false, message:error.message})
            
        }
}