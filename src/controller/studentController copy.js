import { Student } from "../schema/model.js"

export let getAllStudents = async(req,res,next) => {
    try {
        let result = await Student.find()
        res.json({
            success:true,
             message:"Student Read successfully",
             result})
    } catch (error) {
        res.json({
            success:false,
            message:error
        })
    }

  
}
export let createStudent = async(req,res,next) => {
    // console.log(req.body)
    let data = req.body
    
    try {
     let result = await Student.create(data)
     res.json({success:true, message:"Student create successfully",result})
    } catch (error) {
     res.json({success:false, message:error.message})
    }
    
     
 }
 export let getStudentById = async(req, res, next)=> {
    let id = req.params.id
    //console.log("id is: ",id)
    try {
        let result = await Student.findById(id)
        res.json({success:true, message:"Student Read successfully",result:result})
    } catch (error) {
       // res.json({success:false, message:error.message})
    }
   
   
}

export let deleteStudentById = async(req,res,next) => {
    let id = req.params.id
    try {
        let result = await Student.findByIdAndDelete(id)
        if (result === null){
            res.json({
                success: false,
                message: "No student with the given ID was found."
            })
        }else {
        res.json({success:true, message:"Student deleted successfully",result})

        }

    } catch (error) {
        res.json({success:false, message:error.message})
        
    }
   
}

export let updateStudentById = async(req,res,next) => {
    let id = req.params.id
    let data = req.body
    try {
        let result = await Student.findByIdAndUpdate(id,data,{new: true})
    res.json({success:true, message:"Student Updated successfully",result})


    } catch (error) {
    res.json({success:false, message:error.message})
        
    }
}
/* 
{
    "name": "c",
    "age": 33,
    "password":"Password@123",
    "phoneNumber": 1111111111,
    "roll": 2,
    "isMarried": false,
    "spouseName": "lkjl",
    "email": "abc@gmai",
    "gender": "male",
    "dob": "2019-2-2",
    "location": {
        "country": "nepal",
        "exactLocation": "gagalphedi"
    },
    "favTeacher": [
        "a",
        "b",
        "c",
        "nitan"
    ],



    "favSubject": [
        {
            "bookName": "javascript",
            "bookAuthor": "nitan"
        },
        {
            "bookName": "b",
            "bookAuthor": "b"
        }
    ],



} */
// export let createStudent = 

//  export let getStudentById = 