import { Product } from "../schema/model.js";

export let createProduct = async(req, res, next) => {
    let data = req.body
    try{
        const product = await Product.create(data);
        if(!product){
            return res.status(400).json({success: false, msg:'Failed to add new product'})
            }else{
                console.log('New product added')
                return res.status(201).json({success: true,msg:"product created!",product})
                    }


}
catch (e) {
    console.error(e);
}
};

export let getAllProducts = async(req, res, next) => {
    let limit = req.query.limit
    let page=req.query.page
    try{
        const products =  await Product.find().skip((page-1)*limit).limit(limit); //pagination
        // Sending back a response with the list of products in it
         res.status(200).json({success: true, msg:'product successfully fetched',products});
        } catch (err) {
            res.json({success:false, message:err.message})
            }
}

// get specified products

export let getOneProduct = async (req,res,next)=>{
    let id=req.params.id;
    try{
      
        const product =await Product.findById(id);
        if (!product){
            return res.status(404).json({ success :false ,msg:`No user found with the id ${id}`})
            } else{
                return res.status(200).json({success:true,msg:`product detail fetched successfully`,product})
                
            }
        }
        catch(err){
            console.error(err.message)
        }

}

//update specified product
export let updateProduct=async(req,res,next)=> {
    const id = req.params.id;
    const updates = req.body;
    console.log(updates)
    try {
    let result = await Product.findByIdAndUpdate(id,updates,{new: true})
    res.json({success:true, message:"product Updated successfully",result})


    } catch (error) {
        console.log("error ",updates)
    res.json({success:false, message:error.message})
        
    }
    }

    //delete specified product
    export let deleteProduct =async (req,res,next) =>{
        const id = req.params.id;
        try {
            let result = await Product.findByIdAndDelete(id)
            if (result === null){
                res.json({
                    success: false,
                    message: "No product with the given ID was found."
                })
            }else {
            res.json({success:true, message:"product deleted successfully",result})
    
            }
    
        } catch (error) {
            res.json({success:false, message:error.message})
            
        }
}