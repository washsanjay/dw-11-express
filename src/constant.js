import { config } from "dotenv";

config()

export let PORT = process.env.PORT
export let MONGO_URL = process.env.MONGO_URL

export let SECRET_KEY = process.env.SECRET_KEY

export let USER = process.env.USER
export let PASS = process.env.PASS
