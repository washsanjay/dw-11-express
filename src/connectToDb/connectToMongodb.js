import mongoose from "mongoose"
import { MONGO_URL } from "../constant.js"

export let connectToMongodb = () => {
    mongoose.connect(MONGO_URL)
   
}