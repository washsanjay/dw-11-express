import { Schema } from "mongoose";

export let reviewSchema = Schema({
        productId:{
        type: Schema.ObjectId,
        ref:"Product",  //referencing to the Product model
        required: true
        },
        userId: {
        type: Schema.ObjectId,
        ref: 'User',   //referencing User model
        required: true

        },
        description: {
        type: String,
        required: true
        }


})