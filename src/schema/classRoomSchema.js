import { Schema } from "mongoose";

export let classRoomSchema = Schema(
    {
        name: {
            type: String,
            required: true,
        },
        numberOfBenches: {
            type: Number,
            required: true,
        },
        hasTv: {
            type: Boolean,
            required: true,
        },
       
    }
)
