import { Schema } from "mongoose";

export let BlogSchema = Schema(
    {
        title: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
       
    }
)
