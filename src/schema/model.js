//defining array is called model

import { model } from "mongoose";
import { studentSchema } from "./studentSchema.js";
import { teacherSchema } from "./teacherSchema.js";
import { classRoomSchema } from "./classRoomSchema.js";
import { collegeSchema } from "./collegeSchema.js";
import { departmentSchema } from "./departmentSchema.js";
import { traineeSchema } from "./traineeSchema.js";
import { contactSchema } from "./contactSchema.js";
import { BlogSchema } from "./blogSchema.js";
import { productSchema } from "./productSchema.js";
import { userSchema } from "./userSchema.js";
import { reviewSchema } from "./reviewSchema.js";
import studentImgSchema from "./studentImgSchema.js";
import webUserSchema from "./webUserSchema.js";

export let Student = model("Student", studentSchema)
export let Blog = model("Blog", BlogSchema)
export let Teacher = model("Teacher", teacherSchema)
export let ClassRoom = model("ClassRoom", classRoomSchema)
export let College = model("College", collegeSchema)
export let Department = model("Department", departmentSchema)
export let Trainee = model("Trainee", traineeSchema)

export let Contact = model("Contact", contactSchema)
export let Product = model("Product", productSchema)
export let User = model("User", userSchema)
export let Review = model("Review", reviewSchema)
export let WebUser = model("WebUser", webUserSchema)
export let StudentImg = model("StudentImg", studentImgSchema)