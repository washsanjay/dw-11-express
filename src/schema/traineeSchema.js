import { Schema } from "mongoose";

export let traineeSchema = Schema(
    {
        name: {
            type: String,
            required: true,
        },
        class: {
            type: Number,
            required: true,
        },
        faculty: {
            type: Number,
            required: true,
        },
       
    }
)
