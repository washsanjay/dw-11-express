import { Schema } from "mongoose";

export let contactSchema = Schema({
        fullName:{
        type: String,
        required: true
        },
        email: {
        type: String,
        required: true

        },
        phoneNumber: {
        type: String,
        required: true
        },
        address:{
        type: String,
        required : true
        }


})