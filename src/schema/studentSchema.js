import { Schema } from "mongoose";

export let studentSchema = Schema(
    {
        name: {
            type: String,
            required: [true,'name field is required'],
        },
       password: {
            type:String,
            required:[true,'password field is required'],
            validate:(value)=>{
                let isValidPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+{}\[\]:;<>,.?~\\-]).{8,15}$/.test(value)  
                if(!isValidPassword){
                    throw new Error('Please enter a valid password');
                   }   
                        
                      

            }
        }, 
         /* 
        roll: {
            type:Number,
            required:[true,'Roll field is required'],
        },
        isMarried: {
            type:Boolean,
            required:[true,'isMarried field is required'],
        },
        spouseName: {
            type:String,
            required:[true,'spouseName field is required'],
        },*/
        email: {
            type:String,
            required:[true,'Email field is required'],
            unique: true,
            validate:(value)=>{
                let isValidEmail = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(value);
                        if(!isValidEmail){
                         throw new Error('Please enter a valid Email Address');
                        }
                        },


                    },             
                          
       /*  dob: {
            type:Date,
            required:[true,'DOB field is required'],
        },
        gender: {
            type:String,
            required:[true,'gender field is required'],
        },
        location:{
            country :{
                type:String,
                default:"Nepal",
                required:[true,'Country field is required'],
                },
            exactLocation : {
                type:String,
                default:"Bhaktapur" ,
                required:[false,"Exact Location field is not required"],
            }

        },
        favTeacher:[
            {
                type:String,
                 }
        ],
        favSubject:[
            {
                bookName: {type: String},  
                bookAuthor:   {type: String}  
            }
        ],
        favNumber:{
            type:Number,
        
        },
        favPerson:{
            name: {
               type: String,
            },
            age :{
                type: Number
            }
                

        },
        favFood:[{
            name:{
                type: String
            }
        }] */
       
    }
)


/* 
// 1st step of crud
import { Schema } from "mongoose";
export let dataSchema = {
    name: {
        type: String,
        required: [true, "name is required"],
        lowercase: true,
        // uppercase: true,
        trim: true,
        minLength: [3, "name must be at least 3 characters long"],
        maxLength: [30, "name must be at least 30 characters long"],
        validate: (value)=>{
            if(/^[a-zA-Z0-9]+$/.test(value)){
            }else {
                throw new Error("Name should include apha numerical values")
            }
        } // end of validate
    }, // end of name
    password: {
        type: String,
        required: [true, "password is required"],
        trim: true,
        validate: (value)=>{
            if (/^[a-zA-Z0-9]+$/.test(value)){
            }else {
                throw new Error("password should contain alpha numeric")
            }
        } // validate end
    },
    roll: {
        type: Number,
        required: [true, "roll is required"],
        trim: true,
    },
    isMarried: {
        type: Boolean,
        required: [true, "isMarried is required"]
    },
    spouseName: {
        type: String,
        required: [true, "spouseName is required"]
    },
    email: {
        type: String,
        required: [true, "email is required"],
        unique: true,
    },
    gender: {
        type: String,
        required: [true, "gender is required"],
        validate: (value)=>{
            if (value === "male" || value === "female" || value === "other"){
            }else {
                throw new Error("gender should either be male, female or other")
            }
        } // validate end
    },
    phoneNumber: {
        type: String,
        required: [true, "phoneNumber is required"],
        validate: (value)=>{
            if (String(value).length === 10){
            }else {
                throw new Error("gender should either be male, female or other")
            }
        } // validate end
    },
    dob: {
        type: Date,
        required: [true, "dob is required"]
    },
    location: { //object
        country: {
            type: String,
            required: [true, "country is required"]
        },
        exactLocation: {
            type: String,
            required: [true, "exactLocation is required"]
        },
    }, //end of location
    favTeacher: [ //array of strings
        {
            type: String,
        },
    ],
    favSubject: [ //arr of objects
        {
            bookName: {
                type: String,
                required: [true, "bookName is required"]
            },
            bookAuthor: {
                type: String,
                required: [true, "bookAuthor is required"]
            },
        }, // only on object since in arr
    ] // end of favsubject
} // ennd of schema */