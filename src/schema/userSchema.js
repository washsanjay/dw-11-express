import { Schema } from "mongoose";

export let userSchema = Schema({
        name:{
        type: String,
        required: true
        },
        address:{
                type: String,
                required: true
                },
        phoneNumber: {
        type: Number,
        required: true

        },
        email:{
                type: String,
                required: true,
                unique:true
                },
        password:{
               type: String,
               required: true
                        },
        

},{timestamps:true})