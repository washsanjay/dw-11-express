//console.log("My Name is Sanjay")

/* make backend application (using express)
attached port to that application

url : localhost:8001
method: get/post/patch/delete 
*/

import express, { json } from "express"
import { firstRouter } from "./src/route/firstRouter.js";
import { traineesRouter } from "./src/route/traineesRouter.js";
import { schoolsRouter } from "./src/route/schoolsRouter.js";
import { vehiclesRouter } from "./src/route/vehiclesRouter.js";
import { connectToMongodb } from "./src/connectToDb/connectToMongodb.js";
import { studentsRouter } from "./src/route/studentsRouter.js";
import { teachersRouter } from "./src/route/teachersRouter.js";
import { departmentsRouter } from "./src/route/departmentsRouter.js";
import { collegesRouter } from "./src/route/collegesRouter.js";
import { classRoomsRouter } from "./src/route/classRoomsRouter.js";
import { contactRouters } from "./src/route/contactsRouter.js";
import { blogsRouter } from "./src/route/blogsRouter.js";
import { productRouter } from "./src/route/productsRouter.js";
import { userRouter } from "./src/route/usersRouter.js";
import { reviewRouter } from "./src/route/reviewsRouter.js";


import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { PORT, SECRET_KEY } from "./src/constant.js";
import { studentImgRouter } from "./src/route/studentImgsRouter.js";
import { fileRouter } from "./src/route/fileRouter.js";
import { webUserRouter } from "./src/route/webUserRouter.js";


let expressApp = express()
expressApp.use(json())


//application middleware
connectToMongodb()

/* expressApp.use((req, res , next) => {
    console.log('This is a global middleware')
    next();
}) */
expressApp.use("/first",firstRouter)
expressApp.use('/trainees',traineesRouter)
expressApp.use('/teachers',teachersRouter)
expressApp.use('/departments',departmentsRouter)
expressApp.use('/colleges', collegesRouter)
expressApp.use('/classRooms', classRoomsRouter)
expressApp.use('/schools',schoolsRouter)
expressApp.use('/vehicles',vehiclesRouter)
expressApp.use('/students',studentsRouter)
expressApp.use('/contacts',contactRouters)
expressApp.use('/products', productRouter)
expressApp.use('/users', userRouter)
expressApp.use('/reviews', reviewRouter)
expressApp.use('/blogs',blogsRouter)
expressApp.use("/web-users",webUserRouter)//kabaf case
expressApp.use('/studentImgs',studentImgRouter)
expressApp.use('/file',fileRouter)


expressApp.use(express.static("./public"))



//const PORT=8081;



expressApp.listen(PORT , ()=>{
    console.log(`Server running at http://localhost:${PORT}/`)
}
)

// let password = "abc@123"
// let hashedPassword = await bcrypt.hash(password,10)
// console.log(hashedPassword)  

// let password = "abc@123";
// let hashedPassword = "$2b$10$/gOgSa2ltgmR6kXssV1mWeURYqyzA8JUlrWrptTvBwJ.DeUzMJ.mu"
// let isPasswordMatched = await bcrypt.compare(password, hashedPassword)
// console.log(isPasswordMatched) 

/* let salt = "$2a$10$ZxdP65gq7WBQXGJ9LzCuYwlK4F/jDvNrUmVtIyOEkHbTpAf.";
let userInfo={username:"admin",email:"admin@gmail.com",password:password};
        bcrypt.compare(userInfo.password,hashedPassword).then(result=>console
.log(result)) */


//json web token(generate token)
/* let infoObj = {
  // _id:"12345654321"
  name:"sanjay",
  email:"washsanjay@gmail.com"
}
//let secretKey = "dw11"
let expireInfo = {
    expiresIn : '1h',  //1s ,1m, 365d
    }
let token = jwt.sign(infoObj,SECRET_KEY,expireInfo)
console.log(token) */

// let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoic2FuamF5IiwiZW1haWwiOiJ3YXNoc2FuamF5QGdtYWlsLmNvbSIsImlhdCI6MTcwNjM0MjkwNCwiZXhwIjoxNzA2MzQ2NTA0fQ.tStd9CL8NRrLOhibukmzUpk-giag64pDSiyTyl24Fgo"

// try {
//     let infoObj = jwt.verify(token,'dw11')
//     console.log(infoObj)
// } catch (error) {
//     console.log(error.message)
    
// }
// let unknown = jwt.verify(token,"dw11")
//for token to be validate 
//token must be made from the given secret key
//token must not expire

// it token is valid it gives infoObj error 

//.env
/* 
add
port 
url
credentials (email, password, secret key)

point to remember in .env file
  in .env file we define variable 
  use uppercase convention
  every thing in .env is string
 */
// console.log(process.env.NAME)
// console.log(process.env.AGE)
// console.log(process.env.IS_MARRIED)
//console.log(`http://localhost:${process.env.PORT}/users`)


//multer


/* ()
student = [
    {
        name:"John Doe",
        age:25,
        isMarried: false
    },{
        name:"Jane Smith",
        age:32,
        isMarried: true
          },
          {
            name:"Alice Johnson",
            age:40,
            isMarried:false
            
          }
]

Define Object => Schema
Define array  => MOdel*/